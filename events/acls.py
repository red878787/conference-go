from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests, json, random


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + state}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    return {"picture_url": content["photos"][0]["src"]["original"]}
    # headers = {"Authorization": PEXELS_API_KEY}
    # query = f"{city} {state}"
    # url = f"https://api.pexels.com/v1/search?query={query}&per_page=1"

    # response = requests.get(url, headers=headers)
    # data = response.json()

    # if "photos" in data and len(data["photos"]) > 0:
    #     photo = random.choice(data["photos"])

    #     picture_url = photo["src"]["medium"]

    #     return {"picture_url": picture_url}

    # return {"picture_url": None}


def get_weather_data(city, state):
    geocode_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
    geocode_response = requests.get(geocode_url)
    geocode_data = geocode_response.json()

    if geocode_data:
        lat = geocode_data[0]["lat"]
        lon = geocode_data[0]["lon"]

        weather_url = f"http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
        weather_response = requests.get(weather_url)
        weather_data = weather_response.json()

        main_temp = weather_data["main"]["temp"]
        weather_desc = weather_data["weather"][0]["description"]

        return {"temperature": main_temp, "description": weather_desc}
    else:
        return {"error": "Unable to get weather data"}
